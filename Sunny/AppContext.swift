//
//  AppContext.swift
//  Sunny
//
//  Created by Polidea on 12/10/2017.
//  Copyright © 2017 Polidea. All rights reserved.
//

import Foundation
import UIKit

final class AppContext {

    private lazy var client = HTTPClient()
    private lazy var locationService = LocationService()
    private lazy var customLocationService = CustomLocationService()
    
    lazy var weatherViewController: WeatherViewController = {
        let dataSource = UpdatableArrayDataSource<WeatherRowItem, WeatherServiceError>(
            cellIdentifier: WeatherViewCell.identifier,
            elements: [],
            dataProvider: WeatherService(
                locationService: customLocationService,
                httpClient: client
            ),
            configureCellBlock: { (cell, item) in
                guard let cell = cell as? WeatherViewCell else {
                    return
                }
                cell.update(with: item)
            }
        )

        return WeatherViewController(dataSource: dataSource, customLocationService: customLocationService)
    }()
    
    private lazy var searchLocationViewController: SearchLocationViewController = {
        
        let dataSource = UpdatableArrayDataSource<WeatherRowItem, WeatherServiceError>(
            cellIdentifier: WeatherViewCell.identifier,
            elements: [],
            dataProvider: WeatherService(
                locationService: customLocationService,
                httpClient: client
            ),
            configureCellBlock: { (cell, item) in
                guard let cell = cell as? WeatherViewCell else {
                    return
                }
                cell.update(with: item)
            }
        )
        
        return SearchLocationViewController(dataSource: dataSource, customLocationService: customLocationService) { [weak self] location in
            self?.locationDataProvider.add(item: LocationRowItem(location: location, isCurrent: false))
        }
    }()
    
    private lazy var locationDataProvider = LocationsDataProvider(locationService: locationService)
    
    lazy var locationsViewController: LocationsViewController = {
        let dataSource = UpdatableArrayDataSource<LocationRowItem, LocationServiceError>(
            cellIdentifier: LocationViewCell.identifier,
            elements: [],
            dataProvider: locationDataProvider,
            configureCellBlock: { (cell, item) in
                guard let cell = cell as? LocationViewCell else {
                    return
                }
                cell.update(with: item)
            }
        )
        
        return LocationsViewController(dataSource:dataSource, searchLocationViewController: searchLocationViewController, weatherViewController: weatherViewController)
    }()
}
