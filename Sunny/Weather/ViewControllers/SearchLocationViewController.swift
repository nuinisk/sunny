//
//  SearchLocationViewController.swift
//  Sunny
//
//  Created by Suren Hakobyan on 10/17/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit
import CoreLocation

class SearchLocationViewController: UIViewController {

    typealias CompletionBlock = (_ location: CLLocation) -> Void
    
    // MARK: private properties

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(WeatherViewCell.self, forCellReuseIdentifier: dataSource.cellReuseIdentifier)
            tableView.dataSource = dataSource
            tableView.separatorStyle = .none
            dataSource.registerReloadFinishedCallback({ [weak self] error in
                if let error = error {
                    print("Error while getting the data: \(error)")
                } else {
                    self?.tableView.reloadData()
                }
            })
        }
    }
    
    private let dataSource: UpdatableArrayDataSource<WeatherRowItem, WeatherServiceError>
    private var customLocationService: CustomLocationService
    
    // MARK: public properties
    
    public var completion: CompletionBlock
    
    // MARK: initalizers
    
    init(dataSource: UpdatableArrayDataSource<WeatherRowItem, WeatherServiceError>, customLocationService: CustomLocationService, completion: @escaping CompletionBlock) {
        self.dataSource = dataSource
        self.customLocationService = customLocationService
        self.completion = completion
        
        super.init(nibName: "SearchLocationViewController", bundle: Bundle.main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let doneBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneBarButtonAction))
        navigationItem.rightBarButtonItem = doneBarButtonItem
        
        let cancelBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelBarButtonAction))
        navigationItem.leftBarButtonItem = cancelBarButtonItem
        title = NSLocalizedString("Search", comment: "Search")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchBar.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        dataSource.clear()
        searchBar.text = nil
        customLocationService.location = nil
    }
    
    // MARK: helper methods
    
    @objc func doneBarButtonAction() {
        defer { dismiss(animated: true) }
        guard let location = customLocationService.location else {
            return
        }
        completion(location)
    }
    
    @objc func cancelBarButtonAction() {
        dismiss(animated: true)
    }
}

extension SearchLocationViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let cordinates = searchText.trimmingCharacters(in: CharacterSet(charactersIn: " ")).components(separatedBy: CharacterSet(charactersIn: " "))
        guard cordinates.count == 2, let lat = CLLocationDegrees(cordinates[0]),
        let long = CLLocationDegrees(cordinates[1]) else {
            dataSource.clear()
            return
        }
        
        let location = CLLocation(latitude: lat, longitude: long)
        customLocationService.location = location
        dataSource.reload()
    }
}
