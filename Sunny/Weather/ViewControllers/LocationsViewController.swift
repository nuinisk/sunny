//
//  LocationsViewController.swift
//  Sunny
//
//  Created by Suren Hakobyan on 10/17/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit
import CoreLocation

class LocationsViewController: UIViewController {

    // MARK: private properties
    
    private var refreshControl: UIRefreshControl!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(LocationViewCell.self, forCellReuseIdentifier: dataSource.cellReuseIdentifier)
            tableView.dataSource = dataSource
            tableView.delegate = self
            tableView.separatorStyle = .none
            
            let control = UIRefreshControl()
            refreshControl = control
            control.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
            tableView.addSubview(control)
            dataSource.registerReloadFinishedCallback({ [weak self] error in
                if self?.refreshControl.isRefreshing == true {
                    self?.refreshControl.endRefreshing()
                }
                if let error = error {
                    print("Error while getting the data: \(error)")
                } else {
                    self?.tableView.reloadData()
                }
            })
        }
    }
    
    private let dataSource: UpdatableArrayDataSource<LocationRowItem, LocationServiceError>
    
    private var searchLocationViewController: SearchLocationViewController
    private var weatherViewController: WeatherViewController
    
    // MARK: initalizers
    
    init(dataSource: UpdatableArrayDataSource<LocationRowItem, LocationServiceError>, searchLocationViewController: SearchLocationViewController, weatherViewController: WeatherViewController) {
        self.searchLocationViewController = searchLocationViewController
        self.weatherViewController = weatherViewController
        self.dataSource = dataSource
        super.init(nibName: "LocationsViewController", bundle: Bundle.main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func handleRefresh() {
        dataSource.reload()
    }
    
    func reload() {
        tableView.reloadData()
    }
    
    // MARK: overrided methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let addBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addBarButtonAction))
        navigationItem.rightBarButtonItem = addBarButtonItem
        title = NSLocalizedString("Locations", comment: "Locations")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleRefresh()
    }

    // MARK: helper methods
    
    @objc func addBarButtonAction() {
        let navigationController = UINavigationController(rootViewController: searchLocationViewController)
        present(navigationController, animated: true)
    }
}

extension LocationsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let locationRawItem = self.dataSource.elements[indexPath.row]
        weatherViewController.customLocationService.location = locationRawItem.location
        navigationController?.pushViewController(weatherViewController, animated: true)
    }
}
