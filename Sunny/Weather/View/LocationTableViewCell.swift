//
//  LocationTableViewCell.swift
//  Sunny
//
//  Created by Suren Hakobyan on 10/17/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

class LocationViewCell: UITableViewCell {
    private let valueLabel = UILabel()
    private let descriptionLabel = UILabel()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupConstraints() {
        [valueLabel, descriptionLabel].forEach {
            self.contentView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        let verticalInset: CGFloat = 8
        let horizontalInset: CGFloat = 12
        let constraints = [
            valueLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: horizontalInset),
            valueLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: verticalInset),
            valueLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -verticalInset),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -horizontalInset),
            descriptionLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: verticalInset),
            descriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -verticalInset),
            descriptionLabel.leadingAnchor.constraint(lessThanOrEqualTo: valueLabel.trailingAnchor, constant: 10)
        ]
        constraints.forEach { $0.isActive = true }
    }
}

extension LocationViewCell {
    func update(with item: LocationRowItem) {
        valueLabel.text = "\(item.location.coordinate.latitude) \(item.location.coordinate.longitude)"
        descriptionLabel.text = item.isCurrent ? "Current" : nil
    }

    static var identifier: String {
        return String(describing: LocationViewCell.self)
    }
}
