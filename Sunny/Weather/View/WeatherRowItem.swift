//
//  WeatherRowItem.swift
//  Sunny
//
//  Created by Polidea on 12/10/2017.
//  Copyright © 2017 Polidea. All rights reserved.
//

import Foundation

struct WeatherRowItem {
    let title: String
    let value: String
}
