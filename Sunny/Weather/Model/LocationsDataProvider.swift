//
//  LocationsDataProvider.swift
//  Sunny
//
//  Created by Suren Hakobyan on 10/18/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation
import CoreLocation

final class LocationsDataProvider: ArrayDataProvider {
    private let locationService: LocationService
    
    typealias LocationRowItemsObserver = (Result<[LocationRowItem], LocationServiceError>) -> Void

    var observer: LocationRowItemsObserver?
    var elements = [LocationRowItem]()
    
    init(locationService: LocationService) {
        self.locationService = locationService
    }

    func registerDataObserver(_ observer: @escaping LocationRowItemsObserver) {
        self.observer = observer
    }
    
    func refreshData() {
       locationService.enable { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case let .error(error):
                print("Error while getting location data: \(error)")
            case let .success(location):
                
                if self.elements.count > 0 && self.elements[0].isCurrent {
                    self.elements[0] = LocationRowItem(location: location, isCurrent: true)
                } else {
                    self.elements.insert(LocationRowItem(location: location, isCurrent: true), at: 0)
                }
            }
            self.locationService.disable()
            let mapped = result.mapBoth( { _ in self.elements }, right: { LocationServiceError.locationManagerFailed($0) })

            self.observer?(mapped)
        }
    }
    
    func add(item:LocationRowItem) {
        elements.append(item)
        self.observer?(.success(elements))
    }
}
