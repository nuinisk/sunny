//
//  LocationRowItem.swift
//  Sunny
//
//  Created by Suren Hakobyan on 10/17/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation
import CoreLocation

struct LocationRowItem {
    let location: CLLocation
    var isCurrent: Bool
}
