//
//  CustomLocationService.swift
//  Sunny
//
//  Created by Suren Hakobyan on 10/17/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import CoreLocation

class CustomLocationService: LocationService {

    var location: CLLocation?
    
    override var lastLocation: CLLocation? {
        return self.location
    }
    
    override var isEnabled: Bool  {
        return true
    }
}
